const student = {
  name: "",
  lastName: "",
};

student.table = {};

student.name = prompt("Enter your name");
student.lastName = prompt("Enter your last name");

console.log(student);

let discipline = prompt("Enter discipline");
let mark = +prompt("Enter mark");

while (discipline && mark) {
  student.table[discipline] = mark;

  discipline = prompt("Enter discipline");
  mark = +prompt("Enter mark");
}

console.log(student);

let i = 0;
let count = 0;
let markSum = 0;

for (let key in student.table) {
  markSum += student.table[key];
  if (student.table[key] < 4) {
    i++;
  }
  count++;
}

console.log(`Количество плохих оценок по предметам ${i}.`);

if (i === 0) alert("Студент переведено на наступний курс");

console.log(`Средний балл студента ${Math.floor(markSum / count)}.`);

if (markSum / count > 7) alert("Студенту назначено стипендию");
